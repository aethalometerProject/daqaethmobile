import pandas as pd
import os
import time
from datetime import datetime


def generatorPeaks(driveDate, rollWindow, SDMult, SDMultPeak, nOC, peakAgree):

    # -------------------------------------------------------------------------
    # initialization work

    # variable cleanup
    SDs1 = int(SDMult * 10)
    SDs1 = str(SDs1)
    SDs2 = int(SDMultPeak * 10)
    SDs2 = str(SDs2)

    ns1 = str(nOC)
    ns2 = str(peakAgree)

    # change format of the datetime
    driveDate = str(driveDate)
    driveDTO = datetime.strptime(driveDate, '%Y%m%d')
    driveDate = datetime.strftime(driveDTO, '%Y-%m-%d')

    # begin timer for display of elapsed time
    tTotal = time.time()

    # get the current working directory
    cwd = os.getcwd()

    # -------------------------------------------------------------------------
    # data pathing

    dataPathIN = os.path.expanduser(
        cwd + '/dynamic/data/03_baselined/' + driveDate + '_Analysis-L-B' + SDs1 + '.csv')
    dataPathOUT = os.path.expanduser(cwd + '/dynamic/data/04_peakID/' + driveDate +
                                     '_Analysis-L-B' + SDs1 + '-P' + SDs2 + '-O' + ns1 + '-A' + ns2 + '.csv')
    reportPath = os.path.expanduser(cwd + '/dynamic/data/04_peakID/' + driveDate +
                                    '_Analysis-L-B' + SDs1 + '-P' + SDs2 + '-O' + ns1 + '-A' + ns2 + '_REPORT.txt')

    # -------------------------------------------------------------------------
    # data loading

    data = pd.read_csv(dataPathIN, header=0)
    cols = ['DATETIME', 'DT', 'AE33', 'Lat', 'Long', 'LOC', 'DT_rollAvg', 'DT_rollSd', 'AE_rollAvg',
            'AE_rollSd', 'DTadjusted', 'AEadjusted', 'DTchangeBool', 'AEchangeBool', 'DT_baseAvg', 'AE_baseAvg']
    data.columns = cols

    # -------------------------------------------------------------------------
    # BASELINE: PEAK IDENTIFICATION
    # run an identification of peaks .. actual DT/AE values compared with baseline

    # new columns for booleans (is peak? is not peak?)
    data['AEpeakBool'] = int(0)
    data['DTpeakBool'] = int(0)

    data['AEperBase'] = data['AE33'] / data['AE_baseAvg']
    data['DTperBase'] = data['DT'] / data['DT_baseAvg']

    countAEpeak = 0
    countDTpeak = 0

    t = time.time()

    # build python list for index values
    AEpeakRows = []
    DTpeakRows = []

    # for i in range(rollWindow - 1,1400):
    for i in range(len(data)):
        # print(i)

        # VARIABLE DECLARATIONS (AE33)
        iAE = data['AE33'].iloc[i]            # aeth actual
        # aeth roll avg (arithmetic mean) run vs
        iAEavg = data['AE_baseAvg'].iloc[i]
        iAEsd = data['AE_rollSd'].iloc[i]       # aeth roll sd
        # aeth abs(actual - baseline)  ..  ie, distance
        iAEdifAbs = abs(iAE - iAEavg)
        # aeth lower sd bound (sd * multiple)
        lowBdA = SDMultPeak * iAEsd

        # VARIABLE DECLARATIONS (DT)
        iDT = data['DT'].iloc[i]              # pm2.5 actual
        # pm2.5 roll avg (arithmetic mean) run vs
        iDTavg = data['DT_baseAvg'].iloc[i]
        iDTsd = data['DT_rollSd'].iloc[i]       # pm2.5 roll sd
        # pm2.5 abs(actual - baseline)  ..   ie, distance
        iDTdifAbs = abs(iDT - iDTavg)
        # pm2.5 lower sd bound (sd * multiple)
        lowBdD = SDMultPeak * iDTsd

        if iAEdifAbs > lowBdA and iAE > 0:
            data['AEpeakBool'].iloc[i] = 1
            devPerAE = iAE / iAEavg
            data['AEperBase'].iloc[i] = str(devPerAE)
            countAEpeak = countAEpeak + 1
            AEpeakRows.append(i)

        if iDTdifAbs > lowBdD and iDT > 0.016:
            data['DTpeakBool'].iloc[i] = 2
            devPerDT = iDT / iDTavg
            data['DTperBase'].iloc[i] = str(devPerDT)
            countDTpeak = countDTpeak + 1
            DTpeakRows.append(i)

    elapsedT = time.time() - t

    #print('peak count AE: ' + str(countAEpeak))
    #print('peak count DT: ' + str(countDTpeak))
    #print(' elapsed time: ' + str(elapsedT) + ' seconds.')

    # -------------------------------------------------------------------------
    # this section will merge runs.

    # if this asks about the nth spot, then the (n-1) spot

    # we move through a column of identified peaks
    # we ask if another peak occurs in the next n elements
    #   if yes  mark all elts btwn current peak and future peak as true .. ie, (AEbool=1) or (DTbool=2)
    #           set index to index of future peak ( loop will repeat as index is < len(data) )
    #
    #   if no

    # build corrected peak column
    data['AEpeakCorrectedBool'] = int(0)
    data['DTpeakCorrectedBool'] = int(0)

    n = nOC

    t = time.time()

    # ----------------------
    # LOOP ACROSS AE33 DATA
    i = 0

    while i < len(data) - n:

        # print()
        #print('i: ' + str(i) + ' with AEpeakBool: ' + str(data['AEpeakBool'].iloc[i]))

        if data['AEpeakBool'].iloc[i] != 1:                # current peak FALSE
            i = i + 1                                      # set index to index plus one

        else:                                              # current peak TRUE
            #print('|peak = TRUE')

            # set the Corrected Val to true
            data['AEpeakCorrectedBool'].iloc[i] = 1

            #print('|look ' + str(n) + ' elements ahead')

            # set the further position for considering related peaks
            endVal = i + n
            #print('|set endVal: ' + str(endVal))

            while endVal > i:

                #print('|AEpeakBool at index ' + str(endVal) + ': ' + str(data['AEpeakBool'].iloc[endVal]))

                # peak does not exist at this elt; minus one and check again
                if data['AEpeakBool'].iloc[endVal] != 1:
                    endVal = endVal - 1

                    if endVal == (i + 1):
                        i = i + n + 1

                else:

                    delta = endVal - i

                    #print('|peak found at index: ' + str(endVal))
                    data['AEpeakCorrectedBool'].iloc[endVal] = 1
                    #print('|need to change prior ' + str(delta) + 'indices' )

                    for jj in range(1, delta):
                        changeLoc = endVal - jj
                        data['AEpeakCorrectedBool'].iloc[changeLoc] = 1

                        #print('|AEpeakBool at index ' + str(changeLoc) + ' set = 1')
                    #print('|completed ')
                    i = i + n + 1
                    #print('new i: ' + str(i))
                    endVal = i
                    #print('new endVal: ' + str(endVal))

    # --------------------
    # LOOP ACROSS DT DATA
    i = 0

    while i < len(data) - n:

        # print()
        #print('i: ' + str(i) + ' with AEpeakBool: ' + str(data['AEpeakBool'].iloc[i]))

        if data['DTpeakBool'].iloc[i] != 2:                # current peak FALSE
            i = i + 1                                      # set index to index plus one

        else:                                              # current peak TRUE
            #print('|peak = TRUE')

            # set the Corrected Val to true
            data['DTpeakCorrectedBool'].iloc[i] = 2

            #print('|look ' + str(n) + ' elements ahead')

            # set the further position for considering related peaks
            endVal = i + n
            #print('|set endVal: ' + str(endVal))

            while endVal > i:

                #print('|AEpeakBool at index ' + str(endVal) + ': ' + str(data['AEpeakBool'].iloc[endVal]))

                # peak does not exist at this elt; minus one and check again
                if data['DTpeakBool'].iloc[endVal] != 2:
                    endVal = endVal - 1

                    if endVal == (i + 1):
                        i = i + n + 1

                else:

                    delta = endVal - i

                    #print('|peak found at index: ' + str(endVal))
                    data['DTpeakCorrectedBool'].iloc[endVal] = 2
                    #print('|need to change prior ' + str(delta) + 'indices' )

                    for jj in range(1, delta):
                        changeLoc = endVal - jj
                        data['DTpeakCorrectedBool'].iloc[changeLoc] = 2

                        #print('|AEpeakBool at index ' + str(changeLoc) + ' set = 1')
                    #print('|completed ')
                    i = i + n + 1
                    #print('new i: ' + str(i))
                    endVal = i
                    #print('new endVal: ' + str(endVal))

    elapsedT = time.time() - t
    #print(' elapsed time: ' + str(elapsedT) + ' seconds.')

    # -------------------------------------------------------------------------
    # RUN IDENTIFICATION: against AE data

    i = 0

    data['AErun'] = int(0)
    AErunCount = 0

    t = time.time()

    while i < len(data):

        if data['AEpeakCorrectedBool'].iloc[i] != 1:          # the AE has no peak
            i = i + 1                                # iterate to next index

        else:                                        # the AE has a peak

            data['AErun'].iloc[i] = 3

            AErunCount = AErunCount + 1

            j = i
            k = 0

            while data['AEpeakCorrectedBool'].iloc[j] != 0:   # look at current index

                j = j + 1
                k = k + 1

            i = i + k

    elapsedT = time.time() - t

    #print('run count AE: ' + str(AErunCount))
    #print('elapsed time: ' + str(elapsedT) + ' seconds.')

    # -------------------------------------------------------------------------
    # RUN IDENTIFICATION: against DT data

    i = 0

    data['DTrun'] = int(0)
    DTrunCount = 0

    t = time.time()

    while i < len(data):

        #print('current index: ' + str(i))

        if data['DTpeakCorrectedBool'].iloc[i] != 2:          # the AE has no peak
            #print('i: ' + str(i))
            #print('current val: ' + str(data['DTpeakBool'].iloc[i]))
            # print('')
            #data['DTrun'] = 0
            i = i + 1                                # iterate to next index

        else:                                        # the AE has a peak

            #print('j: ' + str(j))
            #print('current val: ' + str(data['DTpeakBool'].iloc[i]))
            # print('')

            data['DTrun'].iloc[i] = 4
            DTrunCount = DTrunCount + 1
            j = i
            k = 0
            while data['DTpeakCorrectedBool'].iloc[j] != 0:   # look at current index
                # print('')

                j = j + 1
                k = k + 1
            i = i + k

    elapsedT = time.time() - t

    #print('run count DT: ' + str(DTrunCount))
    #print('elapsed time: ' + str(elapsedT) + ' seconds.')

    # -------------------------------------------------------------------------
    # PEAK AGREEMENT: AE & DT w/in window
    # at each AE peak value ask if DT has peak within a window

    data['PeakAgree'] = int(0)

    n = peakAgree  # how many spaces to look nearby for peak matches

    t = time.time()

    peakAgreeCount = 0

    for i in range(n, len(data) - n):
        if data['AEpeakCorrectedBool'].iloc[i] == 1:

            for j in range(i - n, i + n + 1):

                #print('j: ' + str(j))
                if data['DTpeakCorrectedBool'].iloc[j] == 2:
                    peakAgreeCount = peakAgreeCount + 1
                    data['PeakAgree'].iloc[i] = 5
            # print('')

    elapsedT = time.time() - t

    maxPAcount = (n + 1) * min(countAEpeak, countDTpeak)

    #print('MAX PeakAgree count: ' + str(maxPAcount))
    #print('this MAX is no longer true .. equation needs rewrite')
    #print('PeakAgree count: ' + str(peakAgreeCount))
    #print('      elapsed time: ' + str(elapsedT) + ' seconds.')

    # -------------------------------------------------------------------------
    # identify runs: on the PeakAgree column
    # (this builds a boolean column in the dataframe)

    i = 0

    data['AgreedRun'] = int(0)
    AgreeRunCount = 0

    t = time.time()

    while i < len(data):

        #print('current index: ' + str(i))

        if data['PeakAgree'].iloc[i] != 5:          # the AE has no peak
            #print('i: ' + str(i))
            #print('current val: ' + str(data['PeakAgree'].iloc[i]))
            # print('')
            #data['DTrun'] = 0
            i = i + 1                                # iterate to next index

        else:                                        # the AE has a peak

            #print('j: ' + str(j))
            #print('current val: ' + str(data['PeakAgree'].iloc[i]))
            # print('')

            data['AgreedRun'].iloc[i] = 6
            AgreeRunCount = AgreeRunCount + 1
            j = i
            k = 0
            while data['PeakAgree'].iloc[j] != 0:   # look at current index
                # print('')

                j = j + 1
                k = k + 1
            i = i + k

    elapsedTTotal = time.time() - tTotal

    #print('AgreeRun count: ' + str(AgreeRunCount))
    #print('  elapsed time: ' + str(elapsedT) + ' seconds.')

    # -------------------------------------------------------------------------
    # EXIT: write out data (csv)

    data.to_csv(dataPathOUT, index=None, header=True)

    # print('[INFO]  Peak Identification complete ..')
    # print('[INFO]  Total elapsed time:', elapsedTTotal)

    # -------------------------------------------------------------------------
    # EXIT: write out report (txt)

    with open(reportPath, 'w'):
        pass

    L = open(reportPath, 'a+')

    s = ('----------------------------------------')

    L.write("\n" + "\n" + str(s) * 2)
    L.write("\n" + "[INFO] print datetime here")
    L.write("\n" + "[INFO] report for data-file: " + str(dataPathOUT))
    L.write("\n" + str(s) * 2 + "\n")
    L.write("\n" + "Window size: " + str(rollWindow) + "\n")
    L.write("\n" + "PEAK IDENTIFICATION")
    L.write("\n" + "SD mult for peak identification: " + str(SDMultPeak))
    L.write("\n" + "(uncorrected) peak count AE: " + str(countAEpeak))
    L.write("\n" + "(uncorrected) peak count DT: " + str(countDTpeak) + "\n")
    L.write("\n" + "run count AE: " + str(AErunCount))
    L.write("\n" + "run count DT: " + str(DTrunCount) + "\n")
    L.write("\n" + " count of peaks in agreement: " + str(peakAgreeCount))
    L.write("\n" + "count of runs in agreedPeaks: " + str(AgreeRunCount) + "\n")
    L.write("\n" + "END ..")
    L.close()
