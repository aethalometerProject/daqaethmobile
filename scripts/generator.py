import os
from datetime import datetime
import pandas as pd
import time

pd.options.mode.chained_assignment = None


def generator(driveDate, rollWindow, SDMult, SDMultPeak):

    # print('[INFO]  Baseline processing for ' + driveDate + ' begins ..')
    cwd = os.getcwd()
    tTotal = time.time()

    # -----------------------------------------------------------------------------
    # preparation work for loading data

    # change order of the given multiple; make SD values into strings
    SDs1 = int(SDMult * 10)
    SDs1 = str(SDs1)
    SDs2 = int(SDMultPeak * 10)
    SDs2 = str(SDs2)

    # change format of the datetime
    driveDate = str(driveDate)
    driveDTO = datetime.strptime(driveDate, '%Y%m%d')
    driveDate = datetime.strftime(driveDTO, '%Y-%m-%d')

    # build pathing arguments for the given date
    dataPathIN = os.path.expanduser(
        cwd + '/static/data/02_locationed/' + driveDate + '_Analysis-L' + '.csv')
    dataPathOUT = os.path.expanduser(
        cwd + '/dynamic/data/03_baselined/' + driveDate + '_Analysis-L-B' + SDs1 + '.csv')
    reportPath = os.path.expanduser(
        cwd + '/dynamic/data/03_baselined/' + driveDate + '_Analysis-L-B' + SDs1 + '_REPORT.txt')

    # put a delete mechanism here for removing files in 03_baselined for the given date

    # -----------------------------------------------------------------------------
    # data loading section

    data = pd.read_csv(dataPathIN, header=0)
    cols = ['DATETIME', 'DT', 'AE33', 'Lat', 'Long', 'LOC']
    data.columns = cols

    # -----------------------------------------------------------------------------
    # calculate the rolling mean and SD

    data['DT_rollAvg'] = data['DT'].rolling(
        window=rollWindow, center=True).mean()
    data['DT_rollSd'] = data['DT'].rolling(
        window=rollWindow, center=True).std()

    data['AE_rollAvg'] = data['AE33'].rolling(
        window=rollWindow, center=True).mean()
    data['AE_rollSd'] = data['AE33'].rolling(
        window=rollWindow, center=True).std()

    # build duplicates of the actual data; dupe created to avoid messing with actual data
    data['DTadjusted'] = data['DT']
    data['AEadjusted'] = data['AE33']

    # -----------------------------------------------------------------------------
    # fix the boundary cases

    # VARIABLES:     need these windows to define the intervals that are the front and end bdys
    rollWindowLow = int(rollWindow / 2)
    rollWindowHigh = int(len(data) - (rollWindow / 2) + 1)

    # loop giving averaged data for the STARTING boundary
    t = time.time()
    j = int(rollWindowLow - 1)

    for i in range(0, rollWindowLow):

        data['DT_rollAvg'].iloc[i] = data["DT"].iloc[0:j].mean()
        data['DT_rollSd'].iloc[i] = data["DT"].iloc[0:j].std()
        #data['DTg_rollAvg'].iloc[i] = data["DT"].iloc[0:j].apply(gmean)

        data['AE_rollAvg'].iloc[i] = data["AE33"].iloc[0:j].mean()
        data['AE_rollSd'].iloc[i] = data["AE33"].iloc[0:j].std()

        j = j + 1

    elapsedT = time.time() - t
    #print('time for averaging on FRONT bdy:', elapsedT)

    # loop giving averaged data for the ENDING boundary

    t = time.time()
    j = int(rollWindowHigh)

    for i in range(rollWindowHigh, len(data)):

        data['DT_rollAvg'].iloc[i] = data["DT"].iloc[j:len(data)].mean()
        data['DT_rollSd'].iloc[i] = data["DT"].iloc[j:len(data)].std()
        #data['DTg_rollAvg'].iloc[i] = data["DT"].iloc[j:len(data)].apply(gmean)

        data['AE_rollAvg'].iloc[i] = data["AE33"].iloc[j:len(data)].mean()
        data['AE_rollSd'].iloc[i] = data["AE33"].iloc[j:len(data)].std()

        j = j + 1

    elapsedT = time.time() - t
    #print('time for averaging on BACKEND bdy:', elapsedT)

    # -----------------------------------------------------------------------------
    # ADJUSTMENT -- remove huge peaks; avoids skewing during baseline calculation
    # run a replacement against DT and AE adjusted
    # DT and AE adjusted are simple copies of their actual values
    # we iterate over the length of the dataframe
    # at ith step ask if abs( actual_i - rollingAverage_i ) > ( SDMult * rollingSd_i)
    # if yes, replace the value in ith row of the adjusted column with rollAvg
    # if no, do nothing. the ith row of the adjusted col remains the actual value

    data['DTchangeBool'] = int(0)
    data['AEchangeBool'] = int(0)

    countAE = 0
    countDT = 0
    t = time.time()

    # build python list for index values
    AEadjustsRows = []
    DTadjustsRows = []

    for i in range(len(data)):

        iAE = data['AE33'].iloc[i]
        iAEavg = data['AE_rollAvg'].iloc[i]
        iAEsd = data['AE_rollSd'].iloc[i]
        iAEdifAbs = abs(iAE - iAEavg)
        iAEdifNeg = (-1) * iAEdifAbs
        lowBdA = SDMult * iAEsd

        iDT = data['DT'].iloc[i]              # pm2.5 actual
        # pm2.5 roll avg (arithmetic mean)
        iDTavg = data['DT_rollAvg'].iloc[i]
        iDTsd = data['DT_rollSd'].iloc[i]       # pm2.5 roll sd
        # pm2.5 abs(actual - roll avg)
        iDTdifAbs = abs(iDT - iDTavg)

        # pm2.5 (roll avg - iDT) .. should be (-1)*iDTdifAbs
        iDTdifNeg = (-1) * iDTdifAbs

        # pm2.5 lower sd bound (sd * multiple)
        lowBdD = SDMult * iDTsd

        if iAEdifAbs > lowBdA:
            data['AEadjusted'].iloc[i] = iAEavg
            data['AEchangeBool'].iloc[i] = 1
            countAE = countAE + 1
            AEadjustsRows.append(i)

        if iAE < 0:
            if iAEdifNeg < (-1) * lowBdA:
                data['AEadjusted'].iloc[i] = (-1) * abs(iAEavg)
                data['AEchangeBool'].iloc[i] = 1
                countAE = countAE + 1
                AEadjustsRows.append(i)

        if iDTdifAbs > lowBdD:
            data['DTadjusted'].iloc[i] = iDTavg
            data['DTchangeBool'].iloc[i] = 2
            countDT = countDT + 1
            DTadjustsRows.append(i)

    elapsedT = time.time() - t

    #print('time for adjusting huge peaks:', elapsedT)

    # -----------------------------------------------------------------------------
    # BASELINE: run rolling average and rolling standard deviation to build baseline at discrete pts

    # calc baseline by roll avg; calc baseline SD vy roll SD; do for DT and AE; new cols for each calc
    data['DT_baseAvg'] = data['DTadjusted'].rolling(
        window=rollWindow, center=True).mean()
    data['AE_baseAvg'] = data['AEadjusted'].rolling(
        window=rollWindow, center=True).mean()

    # -----------------------------------------------------------------------------
    # BASELINE: ROLL AVG FRONT-END
    # bc the baseline is built using rolling average the front- and back-end require fixing
    # here we move through the front-end data, using a GROWING number of lines per calc
    # i.e., at row 0 use first 300 rows, [0,299]
    # at row 5 use first 5 rows and next 300 rows [0,304]
    # at row 299 use first 300 rows and next 300 rows [0,599] ie [ [0,299], [300,599] ]

    t = time.time()

    j = int(rollWindowLow - 1)
    for i in range(0, rollWindowLow):

        data['DT_baseAvg'].iloc[i] = data["DTadjusted"].iloc[0:j].mean()
        #data['DT_baseSd'].iloc[i] = data["DTadjusted"].iloc[0:j].std()

        data['AE_baseAvg'].iloc[i] = data["AEadjusted"].iloc[0:j].mean()
        #data['AE_baseSd'].iloc[i] = data["AEadjusted"].iloc[0:j].std()

        j = j + 1

    elapsedT = time.time() - t
    #print('time for roll avg on front end:', elapsedT)

    # BASELINE: ROLL AVG BACK-END
    # bc the baseline is built using rolling average the front- and back-end require fixing
    # here we move through the back-end data, using a SHRINKING number of lines per calc
    # i.e., at row (end - 300) use preceeding 300 rows and next 600 rows, [(end - 600),end]
    # at row (end - 5) use preceeding 300 rows and next 5 rows, [(end - 305),end]
    # at row end use preceeding 300 rows [(end - 300),end]

    t = time.time()

    j = int(rollWindowHigh)
    for i in range(rollWindowHigh, len(data)):

        data['DT_baseAvg'].iloc[i] = data["DTadjusted"].iloc[j:len(
            data)].mean()
        #data['DT_baseSd'].iloc[i] = data["DTadjusted"].iloc[j:len(data)].std()

        data['AE_baseAvg'].iloc[i] = data["AEadjusted"].iloc[j:len(
            data)].mean()
        #data['AE_baseSd'].iloc[i] = data["AEadjusted"].iloc[j:len(data)].std()

        j = j + 1

    elapsedT = time.time() - t
    #print('time for roll avg on back end:', elapsedT)

    # -----------------------------------------------------------------------------
    # write out total elapsed time; setup the report pathing
    elapsedTTotal = time.time() - tTotal

    data.to_csv(dataPathOUT, index=None, header=True)
    # print('[INFO]  Baseline processing complete ..')
    # print('[INFO]  Total elapsed time:', elapsedTTotal)
    # -----------------------------------------------------------------------------
    with open(reportPath, 'w'):
        pass

    L = open(reportPath, 'a+')

    s = ('----------------------------------------')

    L.write("\n" + "\n" + str(s) * 2)
    L.write("\n" + "[INFO] print datetime here")
    L.write("\n" + "[INFO] report for data-file: " + str(dataPathOUT))
    L.write("\n" + str(s) * 2 + "\n")
    L.write("\n" + "Window size: " + str(rollWindow) + "\n")
    L.write("\n" + "CONDITIONAL SWAPPING")
    L.write("\n" + "SD mult for swap: " + str(SDMult))
    L.write("\n" + "swap count on AE: " + str(countAE))
    L.write("\n" + "swap count on DT: " + str(countDT) + "\n")
    L.write("\n" + "total time elapsed: " +
            str(elapsedTTotal) + " seconds" + "\n")
    L.write("\n" + "END ..")
    L.close()
