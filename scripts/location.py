# LIBRARIES: dependencies are loaded
import pandas as pd
import csv
import os
from datetime import datetime, timedelta
import time
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

dayss = mdates.DayLocator()      # every day
months = mdates.MonthLocator()  # every month

# ADD LOCATION DATA

# RP_lat = [40.7241142436223,40.7905642187918] # Rose Park
RP_lat = [40.7241142436223, 40.792]  # Rose Park
RP_long = [-111.942633567414, -111.910450501716]  # Rose Park
DT_lat = [40.7606241806601, 40.77788407]  # Downtown
DT_long = [-111.9104505, -111.8882173]  # Downtown
SH1_lat = [40.71968701, 40.76829676]  # Sugarhouse
SH1_long = [-111.8882173, -111.8391609]  # Sugarhouse
SH2_lat = [40.72553951, 40.76062418]  # Sugarhouse 2
SH2_long = [-111.9013201, -111.8882173]  # Sugarhouse 2
SSL1_lat = [40.71968701, 40.72553951]  # South Salt Lake 1
SSL1_long = [-111.9002515, -111.8916769]  # South Salt Lake 1
SSL2_lat = [40.6836361, 40.71968701]  # South Salt Lake 2
SSL2_long = [-111.91045, -111.8715457]  # South Salt Lake 2
MC_lat = [40.68708105, 40.70726814]  # Millcreek
MC_long = [-111.8715457, -111.8154502]  # Millcreek
HL_lat = [40.63299995, 40.68708105]  # Holladay
HL_long = [-111.8464782, -111.8195493]  # Holladay
CH1_lat = [40.62124771, 40.63299995]  # Cottonwood
CH1_long = [-111.846482, -111.8295822]  # Cottonwood
CH2_lat = [40.61650207, 40.62950754]  # Cottonwood
CH2_long = [-111.8706633, -111.846482]  # Cottonwood
MY_lat = [40.62950754, 40.65818081]  # Murray
MY_long = [-111.9292782, -111.8577737]  # Murray
WV_lat = [40.64988464, 40.72411424]  # Taylorsville
WV_long = [-111.9663998, -111.9292782]  # Taylorsville

# load the data; fix data column names
# dates = ["2019-01-16","2019-01-25","2019-01-26","2019-01-27",
#             "2019-01-28","2019-01-29","2019-01-30","2019-02-01",
#            "2019-02-07","2019-02-08","2019-02-11","2019-02-12"]


for j in range(len(dates)):
    dataPath = os.path.expanduser(
        '~/static/data/01_raw/' + dates[j] + '_Analysis.csv')
    data = pd.read_csv(dataPath, header=0)
    cols = ['DATETIME', 'DT', 'AE33', 'Lat', 'Long']
    data.columns = cols
    data['LOC'] = str('NaN')

    for i in range(len(data)):

        latVal = data['Lat'].iloc[i]
        longVal = data['Long'].iloc[i]

        if (RP_lat[0] <= latVal <= RP_lat[1]) and (RP_long[0] <= longVal <= RP_long[1]):
            data['LOC'].iloc[i] = str('RP')

        if (DT_lat[0] <= latVal <= DT_lat[1]) and (DT_long[0] <= longVal <= DT_long[1]):
            data['LOC'].iloc[i] = str('DT')

        if (SH1_lat[0] <= latVal <= SH1_lat[1]) and (SH1_long[0] <= longVal <= SH1_long[1]):
            data['LOC'].iloc[i] = str('SH')

        if (SH2_lat[0] <= latVal <= SH2_lat[1]) and (SH2_long[0] <= longVal <= SH2_long[1]):
            data['LOC'].iloc[i] = str('SH')

        if (SSL1_lat[0] <= latVal <= SSL1_lat[1]) and (SSL1_long[0] <= longVal <= SSL1_long[1]):
            data['LOC'].iloc[i] = str('SSL')

        if (SSL2_lat[0] <= latVal <= SSL2_lat[1]) and (SSL2_long[0] <= longVal <= SSL2_long[1]):
            data['LOC'].iloc[i] = str('SSL')

        if (MC_lat[0] <= latVal <= MC_lat[1]) and (MC_long[0] <= longVal <= MC_long[1]):
            data['LOC'].iloc[i] = str('MC')

        if (HL_lat[0] <= latVal <= HL_lat[1]) and (HL_long[0] <= longVal <= HL_long[1]):
            data['LOC'].iloc[i] = str('HL')

        if (CH1_lat[0] <= latVal <= CH1_lat[1]) and (CH1_long[0] <= longVal <= CH1_long[1]):
            data['LOC'].iloc[i] = str('CH')

        if (CH2_lat[0] <= latVal <= CH2_lat[1]) and (CH2_long[0] <= longVal <= CH2_long[1]):
            data['LOC'].iloc[i] = str('CH')

        if (MY_lat[0] <= latVal <= MY_lat[1]) and (MY_long[0] <= longVal <= MY_long[1]):
            data['LOC'].iloc[i] = str('MY')

        if (WV_lat[0] <= latVal <= WV_lat[1]) and (WV_long[0] <= longVal <= WV_long[1]):
            data['LOC'].iloc[i] = str('WV')

    outPath = os.path.expanduser(
        '~/daqEth19/mobileData/02_locationed/' + dates[j] + '_Analysis-L.csv')
    data.to_csv(outPath, index=None, header=True)


# ADD LOCATION DATA

# RP_lat = [40.7241142436223,40.7905642187918] # Rose Park
RP_lat = [40.7241142436223, 40.792]  # Rose Park
RP_long = [-111.942633567414, -111.910450501716]  # Rose Park
DT_lat = [40.7606241806601, 40.77788407]  # Downtown
DT_long = [-111.9104505, -111.8882173]  # Downtown
SH1_lat = [40.71968701, 40.76829676]  # Sugarhouse
SH1_long = [-111.8882173, -111.8391609]  # Sugarhouse
SH2_lat = [40.72553951, 40.76062418]  # Sugarhouse 2
SH2_long = [-111.9013201, -111.8882173]  # Sugarhouse 2
SSL1_lat = [40.71968701, 40.72553951]  # South Salt Lake 1
SSL1_long = [-111.9002515, -111.8916769]  # South Salt Lake 1
SSL2_lat = [40.6836361, 40.71968701]  # South Salt Lake 2
SSL2_long = [-111.91045, -111.8715457]  # South Salt Lake 2
MC_lat = [40.68708105, 40.70726814]  # Millcreek
MC_long = [-111.8715457, -111.8154502]  # Millcreek
HL_lat = [40.63299995, 40.68708105]  # Holladay
HL_long = [-111.8464782, -111.8195493]  # Holladay
CH1_lat = [40.62124771, 40.63299995]  # Cottonwood
CH1_long = [-111.846482, -111.8295822]  # Cottonwood
CH2_lat = [40.61650207, 40.62950754]  # Cottonwood
CH2_long = [-111.8706633, -111.846482]  # Cottonwood
MY_lat = [40.62950754, 40.65818081]  # Murray
MY_long = [-111.9292782, -111.8577737]  # Murray
WV_lat = [40.64988464, 40.72411424]  # Taylorsville
WV_long = [-111.9663998, -111.9292782]  # Taylorsville

# load the data; fix data column names
# dates = ["2019-01-16","2019-01-25","2019-01-26","2019-01-27",
#             "2019-01-28","2019-01-29","2019-01-30","2019-02-01",
#            "2019-02-07","2019-02-08","2019-02-11","2019-02-12"]


dates = ["2019-01-25", "2019-01-29"]

for j in range(len(dates)):
    dataPath = os.path.expanduser(
        '~/daqeth19/mobileData/01_raw/' + dates[j] + '_Analysis.csv')
    data = pd.read_csv(dataPath, header=0)
    cols = ['DATETIME', 'DT', 'AE33', 'Lat', 'Long']
    data.columns = cols
    data['LOC'] = str('NaN')

    for i in range(len(data)):

        latVal = data['Lat'].iloc[i]
        longVal = data['Long'].iloc[i]

        if (RP_lat[0] <= latVal <= RP_lat[1]) and (RP_long[0] <= longVal <= RP_long[1]):
            data['LOC'].iloc[i] = str('RP')

        if (DT_lat[0] <= latVal <= DT_lat[1]) and (DT_long[0] <= longVal <= DT_long[1]):
            data['LOC'].iloc[i] = str('DT')

        if (SH1_lat[0] <= latVal <= SH1_lat[1]) and (SH1_long[0] <= longVal <= SH1_long[1]):
            data['LOC'].iloc[i] = str('SH')

        if (SH2_lat[0] <= latVal <= SH2_lat[1]) and (SH2_long[0] <= longVal <= SH2_long[1]):
            data['LOC'].iloc[i] = str('SH')

        if (SSL1_lat[0] <= latVal <= SSL1_lat[1]) and (SSL1_long[0] <= longVal <= SSL1_long[1]):
            data['LOC'].iloc[i] = str('SSL')

        if (SSL2_lat[0] <= latVal <= SSL2_lat[1]) and (SSL2_long[0] <= longVal <= SSL2_long[1]):
            data['LOC'].iloc[i] = str('SSL')

        if (MC_lat[0] <= latVal <= MC_lat[1]) and (MC_long[0] <= longVal <= MC_long[1]):
            data['LOC'].iloc[i] = str('MC')

        if (HL_lat[0] <= latVal <= HL_lat[1]) and (HL_long[0] <= longVal <= HL_long[1]):
            data['LOC'].iloc[i] = str('HL')

        if (CH1_lat[0] <= latVal <= CH1_lat[1]) and (CH1_long[0] <= longVal <= CH1_long[1]):
            data['LOC'].iloc[i] = str('CH')

        if (CH2_lat[0] <= latVal <= CH2_lat[1]) and (CH2_long[0] <= longVal <= CH2_long[1]):
            data['LOC'].iloc[i] = str('CH')

        if (MY_lat[0] <= latVal <= MY_lat[1]) and (MY_long[0] <= longVal <= MY_long[1]):
            data['LOC'].iloc[i] = str('MY')

        if (WV_lat[0] <= latVal <= WV_lat[1]) and (WV_long[0] <= longVal <= WV_long[1]):
            data['LOC'].iloc[i] = str('WV')

    outPath = os.path.expanduser(
        '~/daqEth19/mobileData/02_locationed/' + dates[j] + '_Analysis-L.csv')
    data.to_csv(outPath, index=None, header=True)
