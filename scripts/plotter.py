import os
import pandas as pd


def plotter(SDMult, SDMultPeak, n_OvercountProtect, peakAgreeWindow):

    cwd = os.getcwd()

    # -----------------------------------------------------------------------------
    # initialization work

    # variable cleanup
    SDs1 = int(SDMult * 10)
    SDs1 = str(SDs1)
    SDs2 = int(SDMultPeak * 10)
    SDs2 = str(SDs2)

    ns1 = str(n_OvercountProtect)
    ns2 = str(peakAgreeWindow)

    # declare list of viable dates
    dates = ['2019-01-16', '2019-01-25', '2019-01-26', '2019-01-27', '2019-01-28', '2019-01-29',
             '2019-01-30', '2019-02-01', '2019-02-07', '2019-02-08', '2019-02-11', '2019-02-12']

    RPlist = []
    DTlist = []
    SHlist = []
    SSLlist = []
    MClist = []
    HLlist = []
    CHlist = []
    MYlist = []
    WVlist = []
    WV_bound_list = []

    # -----------------------------------------------------------------------------
    # loop across date values loading datasets using specified parameters

    for j in range(len(dates)):
        dataPath = os.path.expanduser(cwd + '/dynamic/data/04_peakID/' +
                                      dates[j] + '_Analysis-L-B' + SDs1 + '-P' + SDs2 + '-O' + ns1 + '-A' + ns2 + '.csv')
        data = pd.read_csv(dataPath, header=0)

        RP = data[data["LOC"] == "RP"]
        DT = data[data["LOC"] == "DT"]
        SH = data[data["LOC"] == "SH"]
        SSL = data[data["LOC"] == "SSL"]
        MC = data[data["LOC"] == "MC"]
        HL = data[data["LOC"] == "HL"]
        CH = data[data["LOC"] == "CH"]
        MY = data[data["LOC"] == "MY"]
        WV = data[data["LOC"] == "WV"]

        WVdf_head = WV.head(300)
        WVdf_tail = WV.tail(300)
        WVdf_bounds = WVdf_head.append(WVdf_tail, ignore_index=True)

        RPlist.append(RP)
        DTlist.append(DT)
        SHlist.append(SH)
        SSLlist.append(SSL)
        MClist.append(MC)
        HLlist.append(HL)
        CHlist.append(CH)
        MYlist.append(MY)
        WVlist.append(WV)
        WV_bound_list.append(WVdf_bounds)

    # -----------------------------------------------------------------------------
    # construct dataframes

    RPdf = pd.concat(RPlist, axis=0, ignore_index=True)
    DTdf = pd.concat(DTlist, axis=0, ignore_index=True)
    SHdf = pd.concat(SHlist, axis=0, ignore_index=True)
    SSLdf = pd.concat(SSLlist, axis=0, ignore_index=True)
    MCdf = pd.concat(MClist, axis=0, ignore_index=True)
    HLdf = pd.concat(HLlist, axis=0, ignore_index=True)
    CHdf = pd.concat(CHlist, axis=0, ignore_index=True)
    MYdf = pd.concat(MYlist, axis=0, ignore_index=True)
    WVdf = pd.concat(WVlist, axis=0, ignore_index=True)
    WVdf_bounds = pd.concat(WV_bound_list, axis=0, ignore_index=True)

    # -----------------------------------------------------------------------------
    # determine raw peak counts

    RP_PeakCount = RPdf[RPdf.AgreedRun == 6].shape[0]
    DT_PeakCount = DTdf[DTdf.AgreedRun == 6].shape[0]
    SH_PeakCount = SHdf[SHdf.AgreedRun == 6].shape[0]
    SSL_PeakCount = SSLdf[SSLdf.AgreedRun == 6].shape[0]
    MC_PeakCount = MCdf[MCdf.AgreedRun == 6].shape[0]
    HL_PeakCount = HLdf[HLdf.AgreedRun == 6].shape[0]
    CH_PeakCount = CHdf[CHdf.AgreedRun == 6].shape[0]
    MY_PeakCount = MYdf[MYdf.AgreedRun == 6].shape[0]
    WV_PeakCount = WVdf[WVdf.AgreedRun == 6].shape[0]
    WV_bounds_PeakCount = WVdf_bounds[WVdf_bounds.AgreedRun == 6].shape[0]

    # -----------------------------------------------------------------------------
    # Count values .. time and peak

    # Area Times
    RP_time = len(RPdf)
    DT_time = len(DTdf)
    SH_time = len(SHdf)
    SSL_time = len(SSLdf)
    MC_time = len(MCdf)
    HL_time = len(HLdf)
    CH_time = len(CHdf)
    MY_time = len(MYdf)
    WV_time = len(WVdf)
    WV_bounds_time = len(WVdf_bounds)

    Total_time = RP_time + DT_time + SH_time + SSL_time + \
        MC_time + HL_time + CH_time + MY_time + WV_time

    countT = (RP_PeakCount + DT_PeakCount + SH_PeakCount + SSL_PeakCount +
              MC_PeakCount + HL_PeakCount + CH_PeakCount + MY_PeakCount + WV_PeakCount)

    # -----------------------------------------------------------------------------
    # normalization

    # Normalization Constants
    RP_const = RP_time / Total_time
    DT_const = DT_time / Total_time
    SH_const = SH_time / Total_time
    SSL_const = SSL_time / Total_time
    MC_const = MC_time / Total_time
    HL_const = HL_time / Total_time
    CH_const = CH_time / Total_time
    MY_const = MY_time / Total_time
    WV_const = WV_time / Total_time
    WV_bounds_const = WV_bounds_time / Total_time

    # determine ratio [ (PL/PT)/(TL/TT) ]
    alphaRP = (RP_PeakCount / countT) / RP_const
    alphaDT = (DT_PeakCount / countT) / DT_const
    alphaSH = (SH_PeakCount / countT) / SH_const
    alphaSSL = (SSL_PeakCount / countT) / SSL_const
    alphaMC = (MC_PeakCount / countT) / MC_const
    alphaHL = (HL_PeakCount / countT) / HL_const
    alphaCH = (CH_PeakCount / countT) / CH_const
    alphaMY = (MY_PeakCount / countT) / MY_const
    alphaWV = (WV_PeakCount / countT) / WV_const
    alphaWVB = (WV_bounds_PeakCount / countT) / WV_bounds_const

    # normedSum = RP_PeakCount_Norm + DT_PeakCount_Norm + SH_PeakCount_Norm + SSL_PeakCount_Norm + MC_PeakCount_Norm + HL_PeakCount_Norm + CH_PeakCount_Norm + MY_PeakCount_Norm + WV_PeakCount_Norm

    alphaSum = alphaRP + alphaDT + alphaSH + alphaSSL + \
        alphaMC + alphaHL + alphaCH + alphaMY + alphaWV

    # -----------------------------------------------------------------------------
    # PLOTTER

    import matplotlib.pyplot as plt
    plt.rcdefaults()
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.patches as mpatches

    from pylab import MaxNLocator

    blue_patch = mpatches.Patch(color='blue', label='Boundary-Case Peaks')
    green_patch = mpatches.Patch(color='green', label='Core Peaks')

    fig, ax = plt.subplots()

    #leg3 = ax.legend(handles=[blue_patch, green_patch], bbox_to_anchor=(1.05, 0.865), loc=2, borderaxespad=0.)
    # ax.add_artist(leg3)

    locations = ('Rose Park', 'Downtown', 'Sugarhouse', 'South Salt Lake',
                 'Millcreek', 'Holladay', 'Cottonwood', 'Murray', 'West Valley')
    y_pos = np.arange(len(locations))
    performance = [RP_PeakCount, DT_PeakCount, SH_PeakCount, SSL_PeakCount,
                   MC_PeakCount, HL_PeakCount, CH_PeakCount, MY_PeakCount, WV_PeakCount]

    plt.bar(y_pos, performance, align='center', alpha=1.0, color='green')
    plt.bar(8, WV_bounds_PeakCount, align='center', alpha=1.0, color='b')

    plt.xticks(y_pos, locations)
    plt.xticks(rotation=-60, ha="left")
    plt.ylabel('Number of Events')
    plt.title('Observed Wood Burning Events, Mobile Monitoring\n\n')
    # ax.legend()
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
              ncol=2, handles=[green_patch, blue_patch])

    ya = ax.get_yaxis()
    ya.set_major_locator(MaxNLocator(integer=True))

    # plt.show()
    outPNG = os.path.expanduser(cwd + '/dynamic/plots/mobileEvents.png')
    plt.savefig(outPNG, bbox_inches='tight')
    plt.close()

    # -----------------------------------------------------------------------------
    # Normalized Peak Counts

    RP_PeakCount_Norm = alphaRP / alphaSum
    DT_PeakCount_Norm = alphaDT / alphaSum
    SH_PeakCount_Norm = alphaSH / alphaSum
    SSL_PeakCount_Norm = alphaSSL / alphaSum
    MC_PeakCount_Norm = alphaMC / alphaSum
    HL_PeakCount_Norm = alphaHL / alphaSum
    CH_PeakCount_Norm = alphaCH / alphaSum
    MY_PeakCount_Norm = alphaMY / alphaSum
    WV_PeakCount_Norm = alphaWV / alphaSum
    WVB_PeakCount_Norm = alphaWVB / alphaSum

    # -----------------------------------------------------------------------------
    # PLOTTER 2

    performance2 = [RP_PeakCount_Norm, DT_PeakCount_Norm, SH_PeakCount_Norm, SSL_PeakCount_Norm,
                    MC_PeakCount_Norm, HL_PeakCount_Norm, CH_PeakCount_Norm, MY_PeakCount_Norm, WV_PeakCount_Norm]

    green_patch = mpatches.Patch(color='green', label='Core Peaks')

    fig, ax = plt.subplots()

    #leg3 = ax.legend(handles=[blue_patch, green_patch], bbox_to_anchor=(1.05, 0.865), loc=2, borderaxespad=0.)
    # ax.add_artist(leg3)

    plt.bar(y_pos, performance2, align='center', alpha=1.0, color='green')

    plt.xticks(y_pos, locations)
    plt.xticks(rotation=-60, ha="left")
    plt.ylabel('Event Occurrence Rate')
    plt.title(
        'Observed Wood Burning Events, Mobile Monitoring (Time Normalized)\n\n')

    # ax.legend()
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
              ncol=2, handles=[green_patch])

    # plt.show()
    outPNG = os.path.expanduser(cwd + '/dynamic/plots/mobileEventsNormed.png')
    plt.savefig(outPNG, bbox_inches='tight')
    plt.close()

    # -----------------------------------------------------------------------------
    # PLOTTER 3

    performance2 = [RP_PeakCount_Norm, DT_PeakCount_Norm, SH_PeakCount_Norm, SSL_PeakCount_Norm,
                    MC_PeakCount_Norm, HL_PeakCount_Norm, CH_PeakCount_Norm, MY_PeakCount_Norm, WV_PeakCount_Norm]

    blue_patch = mpatches.Patch(color='blue', label='Boundary-Case Peaks')
    green_patch = mpatches.Patch(color='green', label='Core Peaks')

    fig, ax = plt.subplots()

    #leg3 = ax.legend(handles=[blue_patch, green_patch], bbox_to_anchor=(1.05, 0.865), loc=2, borderaxespad=0.)
    # ax.add_artist(leg3)

    plt.bar(8, WVB_PeakCount_Norm, align='center', alpha=1.0, color='blue')
    plt.bar(y_pos, performance2, align='center', alpha=1.0, color='green')

    plt.xticks(y_pos, locations)
    plt.xticks(rotation=-60, ha="left")
    plt.ylabel('Event Occurrence Rate')
    plt.title(
        'Observed Wood Burning Events, Mobile Monitoring (Time Normalized)\n\n')

    # ax.legend()
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
              ncol=2, handles=[green_patch, blue_patch])

    # plt.show()
    outPNG = os.path.expanduser(
        cwd + '/dynamic/plots/mobileEventsNormed_withbounds.png')
    plt.savefig(outPNG, bbox_inches='tight')
    plt.close()
