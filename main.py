'''
This script performs operations on the collected mobile data
'''
# -----------------------------------------------------------------------------

# library imports
import pandas as pd
import csv
import os
from datetime import datetime, timedelta
import time
import numpy as np
from scipy.stats.mstats import gmean
import sys
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from scripts.generator import generator
from scripts.generatorPeaks import generatorPeaks
from scripts.plotter import plotter

dayss = mdates.DayLocator()      # every day
months = mdates.MonthLocator()  # every month

# clear the system terminal window
os.system('cls' if os.name == 'nt' else 'clear')

cwd = os.getcwd()

# -----------------------------------------------------------------------------
# user input section

print('   Welcome to the University of Utah - DAQ Mobile Aethalometer Analysis  ')
print('-------------------------------------------------------------------------------')
print()
print('WHAT AM I:       * This script performs analysis on the collected mobile data')
print('                   - A rolling average smooths the data.')
print('                   - A multiple of the Standard Deviation provides a baseline')
print('                   - A multiple of the Standard Deviation identifies peaks')
print('                   - Overcounting of peaks is protected by looking nearby')
print('                     and peaks found locally are merged')
print('                   - A time window identifies peaks matching between devices')
print()
print('-------------------------------------------------------------------------------')
print('INSTRUCTIONS:    * Please provide values for the indicated parameters.')
print()
print('                 [Rolling Window] .. study value: 600')
print('                  Given as an integer, this value represents the total number ')
print('                  of seconds for the rolling average window.')
print()
print('                 [Standard Deviation Multiple] .. study value: 1.0')
print('                  Given as a float, used to baseline the data ')
print('                  Peaks exceeding (SD*SDMultiple) are dropped for baseline')
print('                  This accomplishes further smoothing by omitting outliers')
print()
print('                 [Standard Deviation (Peak) Multiple] .. study value: 2.5')
print('                  Given as a float, used to identify peaks ')
print('                  Peaks exceeding (SD*PeakMultiple) are counted as peaks')
print()
print('                 [Overcount Protection Value] .. study value: 10')
print('                  Given as an integer, avoids nearby fluctuations in peaks')
print('                  as being counted as unique.')
print('                  i.e., (Y,N,Y,N,Y) would become (Y,Y,Y,Y,Y)')
print('                  Note: this is a time unit in seconds')
print()
print('                 [Peak Agreement Window Value] .. study value: 10')
print('                  Given as an integer, defines window over which peaks are')
print('                  common. I.e., peak on DustTrack and Aethalometer are common')
print('                  Note: this is a time unit in seconds')
print()
print('-------------------------------------------------------------------------------')

# get user date range

rollWindow = input('Rolling window (s): ')
SDMult = input('Standard Deviation Multiple: ')
SDMultPeak = input('Standard Deviation (Peak) Multiple: ')
n_OvercountProtect = input('Overcount Protection Value: ')
peakAgreeWindow = input('Peak Agreement Window Value: ')


# clean up the given user data

rollWindow = int(rollWindow)
SDMult = float(SDMult)
SDMultPeak = float(SDMultPeak)
n_OvercountProtect = int(n_OvercountProtect)
peakAgreeWindow = int(peakAgreeWindow)

# these are temporary variables for use while writing these scripts
# rollWindow = 600
# SDMult = 1.0
# SDMultPeak = 2.5
# n_OvercountProtect = 10
# peakAgreeWindow = 10

print('using the following values:')
#print('driveDate:', driveDate)
print('rollWindow', rollWindow)
print('SDMult:', SDMult)
print('SDMultPeak:', SDMultPeak)
print('n_OvercountProtect:', n_OvercountProtect)
print('peakAgreeWindow:', peakAgreeWindow)
print()
print('-------------------------------------------------------------------------------')
print('[INFO]  Analysis begins ..')

dates = ['20190116', '20190125', '20190126', '20190127', '20190128', '20190129',
         '20190130', '20190201', '20190207', '20190208', '20190211', '20190212']

print('[INFO]  Mobile data baseline begins ..')
print('        This process can take a long time: 5+ min depending upon parameters')
for i in range(len(dates)):
    generator(dates[i], rollWindow, SDMult, SDMultPeak)
print('[INFO]  Mobile data baseline determined ..')
print()

print('[INFO]  Mobile data peak identification begins ..')
print('        This process can take a long time: 5+ min depending upon parameters')
for i in range(len(dates)):
    generatorPeaks(dates[i], rollWindow, SDMult, SDMultPeak,
                   n_OvercountProtect, peakAgreeWindow)
print('[INFO]  Mobile data peaks identified ..')
print()
print('[INFO]  Plotting begins .. ')
plotter(SDMult, SDMultPeak, n_OvercountProtect, peakAgreeWindow)
print('[INFO]  Plotting successful ..')
print('[INFO]  Plot(s) have been saved locally at:')
print('        ' + cwd + '/dynamic/plots/')
print()
print('[INFO]  Script ends.')
