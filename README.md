README file for daqAethMobile Repository

This respository contains code producing results from the mobile
portion of the DAQ-University of Utah Aethalometer Study (2019).

Via terminal run the main.py script to produce plots.

INPUT PARAMETERS:
Rolling Window:
Standard Deviation Multiple:
Standard Deviation Multiple (for peaks):
Overcount Protection Value:
Peak Agreement Window Value:



OUTPUTS (located at /dynamic/plots)
MobileEvents (1 plot)
MobileEventsNormed_withbounds (1 plot)
mobileEventsNormed (1 plot)



DATA INFORMATION
process data is located in the /static/data directory.

DIR: 01_raw
these files contain all of the raw data collected during the mobile survey
pm2.5 (DT) has units (mg/m^3)
AE33 (deltacarbon) has units (ng/m^3)

DIR: 02_locationed
these files contain the raw data with a column appended indicating the location
this column was determined using LAT and LONG data
note: if you want to relocation the data (this takes a long time)
use the the location.py script (will likely need to adjust the path at line 52)




PROCESS DESCRIPTION
when running main.py three function calls are made:
generator.py
generatorPeaks.py
plotter.py
where all of these .py files live in the /scripts/ directory

generator.py
this script creates the rolling averages and rolling standard deviations
relies on the user input parameters (the study used 600s for the window)
removes large peaks (smoothing)
creates baseline for peak identification

generatorPeaks.py
identify individual peaks
identify runs of peaks
identify agreement of peaks

plotter.py
plot results



DATA FILE DESCRIPTION
data files in /static/data/01_raw
columns: (DATETIME,DT,AE33,Lat,Long)
DATETIME.. the date and time stamp at observation
DT.. dusttrack data (pm2.5)
AE33.. deltacarbon. calculated elsewhere
Lat.. latitude
Long.. longitude
LOC.. location (based on region)

data files in /static/data/02_locationed
columns: (DATETIME,DT,AE33,Lat,Long,LOC)
columns include those described above with the following additional:
LOC.. location (based on region)

data files in /dynamic/data/03_baselined
this baselining process is essentially smoothing the data
columns: (DATETIME,DT,AE33,Lat,Long,LOC,DT_rollAvg,DT_rollSd,AE_rollAvg,AE_rollSd,DTadjusted,AEadjusted,DTchangeBool,AEchangeBool,DT_baseAvg,AE_baseAvg
)
note, this dir also contains a report for each date related to script processing
columns include those described above with the following additional:
DT_rollAvg.. rolling average on pm2.5
DT_rollSd.. rolling standard deviation on pm2.5
AE_rollAvg.. rolling average on deltacarbon
AE_rollSd.. rolling standard deviation on deltacarbon
DTadjusted.. if abs value of difference btwn actual and avg dusttrack > multiple of the std dev. set DTadjusted = DTavg
AEadjusted.. if abs value of difference btwn actual and avg deltacarbon > multiple of the std dev. set AEadjusted = AEavg
DTchangeBool.. if DTadjusted was set = DTavg, then put at the same line in this column (used for counting)
AEchangeBool.. if AEadjusted was set = AEavg, then put at the same line in this column (used for counting)
DT_baseAvg.. run new rolling average against the DTadjusted column
AE_baseAvg.. run new rolling average against the AEadjusted column
note: see script for more detailed commentary

data files in /dynamic/data/04_peakID
columns: (DATETIME,DT,AE33,Lat,Long,LOC,DT_rollAvg,DT_rollSd,AE_rollAvg,AE_rollSd,DTadjusted,AEadjusted,DTchangeBool,AEchangeBool,DT_baseAvg,AE_baseAvg,AEpeakBool,DTpeakBool,AEperBase,DTperBase,AEpeakCorrectedBool,DTpeakCorrectedBool,AErun,DTrun,PeakAgree,AgreedRun
)
note: see the actual script for more detailed comments
columns include those described above, with the following additional:
AEpeakBool.. set the row in this col = 1 if abs value of diff btwn actual deltacarbon and AEbaseAvg > SD multiple(for peaks)
DTpeakBool.. set the row in this col = 1 if abs value of diff btwn actual dusttrack and DTbaseAvg > SD multiple(for peaks)
AEperBase.. ratio of measured deltacarbon to smoothed deltacarbon average
DTperBase.. ratio of measured dusttrack (pm2.5) to smoothed dusttrack (pm2.5) average
AEpeakCorrectedBool.. correction on the peakBool column.. if two rows have TRUE but are separated by a number of rows (with number being less than the overcountProtection threshold), then all rows separating those will be marked as TRUE
DTpeakCorrectedBool.. correction on the peakBool column.. if two rows have TRUE but are separated by a number of rows (with number being less than the overcountProtection threshold), then all rows separating those will be marked as TRUE
AErun.. a boolean TRUE placed at the first occurenece of a peakCorrected TRUE.. this gives a count of peaks, regardless of peak length
DTrun.. a boolean TRUE placed at the first occurenece of a peakCorrected TRUE.. this gives a count of peaks, regardless of peak length
PeakAgree.. a boolean TRUE placed whenever peakCorrectedBool is TRUE for both deltacarbon and dusttrack data
AgreedRun.. a boolean TRUE placed at first row when PeakAgree is TRUE


